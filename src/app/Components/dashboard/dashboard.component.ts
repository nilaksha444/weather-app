import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { WeatherApiService } from 'src/app/weather-api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  changeColor = ['#6249cc', '#e19e5f', '#6249cc'];

  public weatherReport;
  public localWeatherReport;
  private weatherReportAdd;
  allCityCodes;
  //addedCityId:string;

  constructor(
    public weatherService: WeatherApiService,
    private router: Router,
    private http: HttpClient
  ) {}

  ngOnInit(): void {
    //make city codes comma seperated string for api reqeust url
    this.getJSON().subscribe((res) => {
      for (let i = 0; i < res['List'].length; i++) {
        //console.log(res['List']);
        //removing no value comma separate space
        if (this.allCityCodes) {
          this.allCityCodes = this.allCityCodes + ',' + res['List'][i].CityCode;
        } else {
          this.allCityCodes = res['List'][i].CityCode;
        }
      }
      this.getWeatherData(this.allCityCodes);
    });
    setInterval(() => {
      this.getWeatherData(this.allCityCodes); 
    }, 50000);
  }

  //parsing city codes to get data
  getWeatherData(allCityCodes) {
    this.weatherService.getCityWeather(allCityCodes).subscribe((res) => {
      this.weatherReport = res['list'];
      localStorage.setItem('weather',JSON.stringify(this.weatherReport));
      let weatherLocalReport = localStorage.getItem('weather');
      this.localWeatherReport = JSON.parse(weatherLocalReport);
      console.log(this.localWeatherReport);
    });
  }
  
  //get cities form local file
  public getJSON(): Observable<any> {
    return this.http.get('/assets/cities.json');
  }

  //navigate to selected card
  selectCity(cityId) {
    this.router.navigateByUrl('city-weather?id=' + cityId);
  }

  //functionality of adding city to dashboard
  addCity(cityname) {
    console.log(cityname.value);
    this.weatherService
      .getCityWeatherByName(cityname.value)
      .subscribe((res) => {
        this.weatherReportAdd = res;
        let addedCityId = this.weatherReportAdd.id;
        console.log(addedCityId);
        this.allCityCodes = this.allCityCodes + ',' + addedCityId;
        this.getWeatherData(this.allCityCodes);
        //console.log(this.allCityCodes);
      });
  }

  //remove selected card - *have to fix
  removeCity(index) {
    this.weatherReport.splice(index, 1);
    console.log(this.weatherReport);
  }
}
