import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { WeatherApiService } from 'src/app/weather-api.service';

@Component({
  selector: 'app-city-weather',
  templateUrl: './city-weather.component.html',
  styleUrls: ['./city-weather.component.scss'],
})
export class CityWeatherComponent implements OnInit {
  selectedWeatherData;

  constructor(
    private rout: ActivatedRoute,
    public weatherService: WeatherApiService
  ) {}

  ngOnInit(): void {
    this.rout.queryParams.subscribe((res) => {
      this.weatherService.getCityWeather(res.id).subscribe((res) => {
        this.selectedWeatherData = res['list'][0];
        console.log(this.selectedWeatherData);
      });
    });
  }
}
