import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class WeatherApiService {
  

  apiKey = 'e840e086e56c58f8e068c93df5b32c2e';
  basePath = 'http://api.openweathermap.org/';
  

  constructor(private http: HttpClient) { }

  //get weather data by city id
  getCityWeather(cityCode: string){
    let dataPath = this.basePath + 'data/2.5/group';
    return this.http.get(dataPath + '?id=' + cityCode + '&units=metric&appid=' + this.apiKey);
  }
  //get weather data by city name
  getCityWeatherByName(cityName: string){
    let dataPath = this.basePath + 'data/2.5/weather'
    return this.http.get(dataPath + '?q='+ cityName + '&units=metric&appid=' + this.apiKey);
  }


}
